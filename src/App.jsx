import './App.css';
import Form from './main/ui/componets/Form/Form.jsx';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import { Routes, Route} from "react-router-dom";
import PasswordManagePage from "./main/ui/componets/PasswordManagePage/PasswordManagePage";

function App() {
    return (
        <div>
            <Routes>
                <Route exact  path={'/login'} element={<Form/>}/>
                <Route exact  path={'/passwords'} element={
                        <PasswordManagePage/>
                }/>
            </Routes>
        </div>
    );
}

export default App;
