import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware  from "redux-thunk";
import { authReducer } from '../reducers/auth.js';
import { appReducer } from '../reducers/app';

const reducersBatch = combineReducers({
    auth: authReducer,
    app: appReducer
});

export const store = createStore(reducersBatch, applyMiddleware(thunkMiddleware));