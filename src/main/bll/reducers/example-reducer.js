import {exampleAPI} from '../../dall/AdminAPI.js';


const initialState = {
    initialized: false,
}



export const exampleReducer = (state= initialState, action) => {
    switch (action.type) {
        case "SET-INITIALIZED":{
            return {...state, initialized: true}
        }
        default:
            return state
    }
}

export  const setInitialized= ()=>{
    return {
        type:'SET-INITIALIZED',
    }
}
export  const  exampleTC = ()=>{
    return (dispatch)=>{
        exampleAPI.logOut()
            .then((response) =>{

            })
            .catch(()=>{

            })
    }
}