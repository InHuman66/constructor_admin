const initialState = {
    errorMessage: "",
    loading: false
}

export const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case ERROR:
            return {
                ...state,
                error: action.payload
            }
        
        case CHANGE_LOADING:
            return {
                ...state,
                loading: action.payload
            }

        default:
            return state
    }
}

//action typesc
const ERROR = "ERROR"
const CHANGE_LOADING = "CHANGE_LOADING"

//action creators
export const errorAction = error => ({ type: ERROR, payload: error.payload })
export const changeLoadingAction = loading => ({ type: CHANGE_LOADING, payload: loading })