import { authentification } from '../../dall/AdminAPI.js';

const initialState = {
    isAuth: false,
    userId: null,
    isLoading: false
}

export const authReducer = (state= initialState, action) => {
    switch (action.type) {
        case LOGOUT: 
            return {
                ...state,
                isAuth: false,
            }

        case LOGIN:
            return {
                ...state,
                isAuth: true,
                userId: action.payload
            }

        case CHANGE_LOADING: 
            return {
                ...state,
                isLoading: action.payload
            }

        case CHECK_AUTH: 
            return {
                ...state,
                isAuth: action.payload
            }

        default:
            return state
    }
}


//action types
const LOGIN = "LOGIN"
const LOGOUT = "LOGOUT"
const CHANGE_LOADING = "CHANGE_LOADING"
const CHECK_AUTH = "CHECK_AUTH"


//action creators
const logoutAction = () => ({type: LOGOUT})
const loginAction = id => ({type: LOGIN, payload: id})
const changeLoading = load => ({type: CHANGE_LOADING, payload: load})
const checkAuthAction = data => ({type: CHECK_AUTH, payload: data})

//middleware
export const logout = () => dispatch => {
    dispatch(changeLoading(true))

    authentification.logOut()
        .then(() => dispatch(logoutAction()))
        .catch(e => console.error(e))
        .finally(() => dispatch(changeLoading(false)))
}

export const login = (email, password) => dispatch => {
    dispatch(changeLoading(true))

    authentification.logIn(email, password)
        .then((res) => {
            dispatch(loginAction(res.data.data.userId))
        })
        .catch(e => console.error(e))
        .finally(() => dispatch(changeLoading(false)))
}

export const checkAuth = () => dispatch => {
    dispatch(changeLoading(true))

    authentification.checkAuth()
        .then((res) =>{
            dispatch(checkAuthAction(!res.data.resultCode))
        })
        .catch(e => console.error(e))
        .finally(() => dispatch(changeLoading(false)))
}

