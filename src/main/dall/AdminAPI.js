import axios from "axios";
// import dotenv from "dotenv";

// dotenv.config()

const instance = axios.create({
    withCredentials: true,
    baseURL: "https://social-network.samuraijs.com/api/1.0/",
    headers: {
        "API-KEY": "abfeea78-6bd5-4197-9663-a5e149720502",
    }
})
export const authentification = {
    logOut(){
        return instance.delete('auth/login')
    },
    logIn(email, password) {
        return instance.post('auth/login', {email, password})
    },
    checkAuth() {
        return instance.get('auth/me')
    }

}