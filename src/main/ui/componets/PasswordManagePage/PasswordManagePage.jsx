import React from 'react';
import classes from './PasswordManagePage.module.scss';
import SideBar from "../SideBar/SideBar";
import Header from "../Header/Header";

const PasswordManagePage = () => {
    return (
        <div className={classes.content_wrapper}>
            <SideBar/>
            <div className={'right_block'}>
                <Header/>
            </div>
        </div>
    );
};
export default PasswordManagePage;
