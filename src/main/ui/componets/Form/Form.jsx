import React from 'react';
import classes from './Form.medule.scss';

const Form = () => {
	return (
		<div className='mainform'>
			<div className='form__content'>
				<div className='form__content__top'>
					<div className='form__content__top-logo'>
						<svg
							width='48'
							height='48'
							viewBox='0 0 48 48'
							fill='none'
							xmlns='http://www.w3.org/2000/svg'
						>
							<circle cx='24' cy='24' r='24' fill='#3751FF' />
							<path
								d='M16.5 14.5C16.5 13.9477 16.9477 13.5 17.5 13.5H23.9857C27.319 13.5 29.9 14.4143 31.7286 16.243C33.5762 18.0716 34.5 20.6475 34.5 23.9705C34.5 27.3132 33.5762 29.9087 31.7286 31.757C29.9 33.5857 27.319 34.5 23.9857 34.5H17.5C16.9477 34.5 16.5 34.0523 16.5 33.5V14.5Z'
								fill='url(#paint0_linear_4856_189)'
							/>
							<defs>
								<linearGradient
									id='paint0_linear_4856_189'
									x1='16.5'
									y1='13.5'
									x2='34.5'
									y2='34.5'
									gradientUnits='userSpaceOnUse'
								>
									<stop stopColor='white' stopOpacity='0.7' />
									<stop offset='1' stopColor='white' />
								</linearGradient>
							</defs>
						</svg>
					</div>
					<div className='form__content__top-text'>Top adminer</div>
				</div>
				<div className='form__content__middle'>
					<div className='form__content__middle-header'>Log In to Adminer</div>
					<div className='form__content__middle-text'>Enter your email and password below</div>
				</div>
				<div className='form__content__form'>
					<div className='form__content__form-email'>
						<p>EMAIL</p>
						<input type='email' name='email' id='email' placeholder='Email address' />
					</div>
					<div className='form__content__form-pass'>
						<div className='form__content__form-passhelp'>
							<p>PASSWORD</p>
							<a href='#'>Forgot password?</a>
						</div>
						<input type='password' name='pass' id='pass' placeholder='Password' />
					</div>
					<div className='form__content__btn'>
						<button>Log In</button>
					</div>
					<div className='form__content__bottom'>
						<p>Don’t have an account?</p>
						<a href='#'>Sign up</a>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Form;
